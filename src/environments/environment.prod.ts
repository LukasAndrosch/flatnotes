export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyBtAb387UXef9rz3TXPuzL9ayKnzUKl7Ks",
    authDomain: "flatnotes-your-flatmanager.firebaseapp.com",
    databaseURL: "https://flatnotes-your-flatmanager.firebaseio.com/",
    projectId: "flatnotes-your-flatmanager",
    storageBucket: "flatnotes-your-flatmanager.appspot.com"
    //messagingSenderId: '<your-messaging-sender-id>'
  }
};
