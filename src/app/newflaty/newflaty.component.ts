import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DialogConfig } from '../dialog/dialog-config';
import { DialogRef } from '../dialog/dialog-ref';
import { DialogService } from '../dialog/dialog.service';
import { MainViewComponent } from '../main-view/main-view.component';
import { AddMemberComponent } from '../add-member/add-member.component';

declare var firebase: any;

@Component({
  selector: 'app-newflaty',
  templateUrl: './newflaty.component.html',
  styleUrls: ['./newflaty.component.css']
})
export class NewflatyComponent implements OnInit {

  messageForm: FormGroup;
  submitted = false;

  alertMessage: String = null;

  constructor(private formBuilder: FormBuilder,
    public config: DialogConfig, public dialog: DialogRef, public dialogService: DialogService) {}

  ngOnInit() {
    this.messageForm = this.formBuilder.group({
      title: ['', Validators.required],
    });
  }

  async onSubmit() {
    this.submitted = true;

    if (this.messageForm.invalid) {
        return;
    }

    let mailHash: any = this.config.data.userMailHash;
    let title: string = this.messageForm.get('title').value;
    
    try{
      //if flatty already exists, promise gets rejected
      await this.checkIfFlatyExists(title);

      //create new flaty and assign the user
      firebase.database().ref("/flaties").child(title).set({members: [mailHash]});

      //add flaty to the user entry
      firebase.database().ref("/users").child(mailHash).update({flaty: title});
      console.log("Flaty created");

      this.dialog.close();
      const ref = this.dialogService.open(MainViewComponent, {data: {userMailHash: mailHash, flaty: title} });

    } catch(error) {
      //database didn't work, or Flaty already existed
      console.log(error);
      this.alertMessage = "Flaty does already exist"
    }
  }

  async checkIfFlatyExists(title: string): Promise<boolean> {
    return new Promise(function(resolve, reject) {
      setTimeout(function() {
        firebase.database().ref("/flaties").on("value", function(snapshot){
          const flaty = snapshot.child(title).val();
          //check whether flaty already exists
          if(flaty !== null) {
            reject(false);
          } else {
            resolve(true);
          }
        });
      }, 100);
    });
  }
}
