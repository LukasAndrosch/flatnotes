import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewflatyComponent } from './newflaty.component';

describe('NewflatyComponent', () => {
  let component: NewflatyComponent;
  let fixture: ComponentFixture<NewflatyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewflatyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewflatyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
