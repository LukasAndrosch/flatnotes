import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlatsquestionComponent } from './flatsquestion.component';

describe('FlatsquestionComponent', () => {
  let component: FlatsquestionComponent;
  let fixture: ComponentFixture<FlatsquestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlatsquestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlatsquestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
