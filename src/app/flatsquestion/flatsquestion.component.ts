import { Component } from '@angular/core';
import { DialogConfig } from '../dialog/dialog-config';
import { DialogRef } from '../dialog/dialog-ref';
import { DialogService } from '../dialog/dialog.service';
import { ExistingflatyComponent } from '../existingflaty/existingflaty.component';
import { NewflatyComponent } from '../newflaty/newflaty.component';

@Component({
  selector: 'app-flatsquestion',
  templateUrl: './flatsquestion.component.html',
  styleUrls: ['./flatsquestion.component.css']
})

export class FlatsquestionComponent {

  constructor(public config: DialogConfig, public dialog: DialogRef, public dialogService: DialogService) {}

  onClose() {
    this.dialog.close('some value');
  }

  public newFlaty(){
    this.dialog.close();
    const ref = this.dialogService.open(NewflatyComponent, this.config );
  }

  public existingFlaty(){
    this.dialog.close();
    const ref = this.dialogService.open(ExistingflatyComponent, this.config);
  }

}
