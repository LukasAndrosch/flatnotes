import { Component, OnInit, Input  } from '@angular/core';

@Component({
  selector: 'app-our-alert',
  templateUrl: './our-alert.component.html',
  styleUrls: ['./our-alert.component.css']
})
export class OurAlertComponent implements OnInit {

  @Input() message: string;

  constructor() { }

  ngOnInit() {
  }

}
