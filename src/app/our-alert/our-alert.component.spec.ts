import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OurAlertComponent } from './our-alert.component';

describe('OurAlertComponent', () => {
  let component: OurAlertComponent;
  let fixture: ComponentFixture<OurAlertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OurAlertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OurAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
