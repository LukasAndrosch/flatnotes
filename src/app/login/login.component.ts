import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DialogConfig } from '../dialog/dialog-config';
import { DialogRef } from '../dialog/dialog-ref';
import { DialogService } from '../dialog/dialog.service';
import { FlatsquestionComponent } from '../flatsquestion/flatsquestion.component';
import { MainViewComponent } from '../main-view/main-view.component';
import { Md5 } from 'ts-md5/dist/md5'

declare var firebase: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  messageForm: FormGroup;
  submitted = false;

  alertMessage: String = null;

  constructor( public config: DialogConfig, public dialog: DialogRef, public dialogService: DialogService,private formBuilder: FormBuilder,
    private router: Router) { }

  ngOnInit() {
    this.messageForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  async onSubmit() {
    this.submitted = true;

    if (this.messageForm.invalid) {
        this.messageForm.reset();
        return;
    }

    const md5 = new Md5();
    let userMail: string = this.messageForm.get('email').value;
    let userMailHash: any = md5.appendStr(userMail).end();
    let passwordHash: any = md5.appendStr(this.messageForm.get('password').value).end();
    
    let exists: boolean = await this.checkIfUserExists(userMailHash);

    //Check if mail is already in use
    if(await exists) {
      let correctPwd: Promise<boolean> = this.checkPassword(userMailHash, passwordHash);
      try {
        await correctPwd;
        this.dialog.close();


        //check whether the user is already in a flaty
        let userInFlaty: Promise<boolean> = this.checkIfInFlaty(userMailHash);
        if(await userInFlaty) {
          let flat: string = await this.getUserFlaty(userMailHash);
          const ref = this.dialogService.open(MainViewComponent, {data: {userMailHash: userMailHash, flaty: flat} });
        } else {
          //not in a flaty -> create or join one
          const ref = this.dialogService.open(FlatsquestionComponent, { data: {userMailHash: userMailHash} });
        }        
      } catch(error) {
        this.alertMessage = "Login failed; Name already in use, or password is wrong";
        console.log(error);
        return;
      }
      
    } else { //Mail not in use -> register new User
      this.dialog.close();
      console.log("New User");
      const ref = this.dialogService.open(FlatsquestionComponent, { data: {userMailHash: userMailHash} });
      
      //Add new User to the database
      firebase.database().ref("/users").child(userMailHash).set({email: userMail, password: passwordHash, flaty: null});
      console.log("added new User");
    }
    this.messageForm.reset();
  }

  async getUserFlaty(userMailHash: any): Promise<string> {
    return new Promise(function(resolve, reject){
      setTimeout(function() {
        firebase.database().ref("/users").on("value", function(snapshot) {
          const flat = snapshot.child(userMailHash).val().flaty;
          resolve(flat);
        });
      }, 100);
    });    
  }

  async checkIfInFlaty(userMailHash: any): Promise<boolean> {
    return new Promise(function(resolve, reject){
      setTimeout(function() {
        firebase.database().ref("/users").on("value", function(snapshot){
          const inFlaty = snapshot.child(userMailHash).val().flaty !== undefined;
          resolve(inFlaty);
        });
      }, 100);
    });    
  }

  async checkIfUserExists(userMail: any): Promise<boolean> {
    return new Promise(function(resolve, reject) {
      setTimeout(function() {
        firebase.database().ref("/users").on("value", function(snapshot) {
          const userExists = snapshot.child(userMail).val() !== null;
          resolve(userExists);
        });
      }, 100);
    }); 
  }

  async checkPassword(userMail: any, password: any): Promise<boolean> {
    return new Promise(function(resolve, reject) {
      setTimeout(function() {
        firebase.database().ref("/users").on("value", function(snapshot) {
        if(snapshot.child(userMail).val().password === password){
          const correctPwd = true;
          resolve(correctPwd);
        } else {
          reject(false);
        }
        });
      }, 100);
    }); 
  }
}
