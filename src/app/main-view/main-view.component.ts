import { Component, OnInit, DebugElement } from '@angular/core';
import { DialogConfig } from '../dialog/dialog-config';
import { DialogRef } from '../dialog/dialog-ref';
import { DialogService } from '../dialog/dialog.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { AddMemberComponent } from '../add-member/add-member.component'


@Component({
  selector: 'app-main-view',
  templateUrl: './main-view.component.html',
  styleUrls: ['./main-view.component.css']
})

export class MainViewComponent implements OnInit {
  userJSON: Observable<any>;

  listTodolistsJSONRef: AngularFireList<any>;
  listTodolistsJSON: Observable<any[]>;

  todolistForm: FormGroup;
  addNewTodoForm: FormGroup;

  selectedTodolistJSONRef: AngularFireList<any>;
  selectedTodolistJSON: Observable<any[]>;

  curListName : String = "";

  constructor(public db: AngularFireDatabase ,public config: DialogConfig,public dialog: DialogRef, public dialogService: DialogService, private formBuilder: FormBuilder) {
    let userMailHash = this.config.data.userMailHash;
    let flaty = this.config.data.flaty;
    console.log(flaty);

   

    this.userJSON = db.object('users/' + userMailHash).valueChanges();

    this.listTodolistsJSONRef = db.list('flaties/' + flaty + "/todos");
    this.listTodolistsJSON = this.listTodolistsJSONRef.valueChanges();
  
  }

  ngOnInit() {
    this.todolistForm = this.formBuilder.group({
      todoName: ['', Validators.required],
    });
    this.addNewTodoForm = this.formBuilder.group({
      newTodo: ['', Validators.required],
    })
  }

  public showMembers(){
    console.log("add member clicked");
    this.dialog.close();
    const ref = this.dialogService.open(AddMemberComponent, this.config );
  }

  public async addTodolist(){
    let _name = this.todolistForm.get('todoName').value;
    this.listTodolistsJSONRef.set(_name , { name : _name});
    this.todolistForm.reset();
  }

  public openTodolist(toOpenList){
    console.log("open todolist:");
    
    let flaty = this.config.data.flaty;
    this.curListName = toOpenList.name;

    this.selectedTodolistJSONRef = this.db.list('flaties/' + flaty + "/todos/" + this.curListName + "/items");
    this.selectedTodolistJSON = this.selectedTodolistJSONRef.valueChanges();

    console.log(this.curListName);
    console.log(flaty);
  }

  public closeList(){
    console.log("close list");
    let flaty = this.config.data.flaty;
    
    this.curListName = "";
    this.addNewTodoForm.reset();

    this.db.object('flaties/' + flaty + "/todos/" + this.curListName).remove();
    this.selectedTodolistJSON = null;
    this.selectedTodolistJSONRef = null;
  }

  public addNewTodo(){
    let _todo = this.addNewTodoForm.get('newTodo').value;
    this.selectedTodolistJSONRef.push({"title" : _todo});
    console.log(_todo);
    this.addNewTodoForm.reset();
  }
}
