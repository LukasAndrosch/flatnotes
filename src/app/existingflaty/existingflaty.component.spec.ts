import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExistingflatyComponent } from './existingflaty.component';

describe('ExistingflatyComponent', () => {
  let component: ExistingflatyComponent;
  let fixture: ComponentFixture<ExistingflatyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExistingflatyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExistingflatyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
