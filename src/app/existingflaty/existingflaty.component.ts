import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DialogConfig } from '../dialog/dialog-config';
import { DialogRef } from '../dialog/dialog-ref';
import { DialogService } from '../dialog/dialog.service';
import { MainViewComponent } from '../main-view/main-view.component';

declare var firebase: any;

@Component({
  selector: 'app-existingflaty',
  templateUrl: './existingflaty.component.html',
  styleUrls: ['./existingflaty.component.css']
})
export class ExistingflatyComponent implements OnInit {
  messageForm: FormGroup;
  submitted = false;

  alertMessage: String = null;

  constructor(private formBuilder: FormBuilder,
    public config: DialogConfig, public dialog: DialogRef, public dialogService: DialogService) {}

  ngOnInit() {
    this.messageForm = this.formBuilder.group({
      flatyId: ['', Validators.required],
    });
  }

  async onSubmit() {
    this.submitted = true;

    if (this.messageForm.invalid) {
        return;
    }

    let mailHash: any = this.config.data.userMailHash;
    let title: string = this.messageForm.get('flatyId').value;
    
    try {
      //Check whether Flaty exists
      let members: any = await this.checkIfFlatyExists(title);
      //Flaty exist
      members.push(mailHash);

      //add new user in flaty and flaty to user
      firebase.database().ref("/users").child(mailHash).update({flaty: title});
      firebase.database().ref("/flaties").child(title).update({members: members});

      this.config.data.flaty = title;
      console.log("[existingFlaty]" + mailHash);
      this.dialog.close();
      this.dialogService.open(MainViewComponent, this.config);
    } catch(error) {
      //Flaty does not exist
      this.alertMessage = "Flaty does no exist"
      console.log(error);
    }
    this.messageForm.reset();
  }

  async checkIfFlatyExists(title: string): Promise<any> {
    return new Promise(function(resolve, reject) {
      setTimeout(function() {
        firebase.database().ref("/flaties").on("value", function(snapshot){
          const flaty = snapshot.child(title).val();
          //check whether flaty already exists
          if(flaty !== null) {
            resolve(flaty.members);
          } else {
            reject(null);
          }
        });
      }, 100);
    });
  }

}
