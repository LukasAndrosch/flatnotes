import { Component } from '@angular/core';
import { DialogService } from './dialog/dialog.service';
import { LoginComponent } from './login/login.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Flatnotes';

  constructor(public dialog: DialogService) {
    const ref = this.dialog.open(LoginComponent, { data: { message: 'I am a dynamic component inside of a dialog!' } });

    ref.afterClosed.subscribe(result => {
      console.log('Dialog closed', result);
    });
  }
}
