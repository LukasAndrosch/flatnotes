import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AngularFireModule } from '@angular/fire';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { MainViewComponent } from './main-view/main-view.component';;
import { FlatsquestionComponent } from './flatsquestion/flatsquestion.component';
import { NewflatyComponent } from './newflaty/newflaty.component';
import { ExistingflatyComponent } from './existingflaty/existingflaty.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { DialogModule } from './dialog/dialog.module';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireDatabase } from 'angularfire2/database';
import { AddMemberComponent } from './add-member/add-member.component';
import {MatListModule} from '@angular/material/list';
import { OurAlertComponent } from './our-alert/our-alert.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainViewComponent,
    FlatsquestionComponent,
    NewflatyComponent,
    ExistingflatyComponent,
    AddMemberComponent,
    OurAlertComponent,
  ],
  imports: [
    BrowserModule,
    MatListModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    DialogModule,
    AngularFireModule.initializeApp(environment.firebase),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [AngularFirestore, AngularFireDatabase],
  bootstrap: [AppComponent],
  entryComponents: [FlatsquestionComponent, MainViewComponent, AddMemberComponent, NewflatyComponent, ExistingflatyComponent ]
})
export class AppModule { }
