import { Component, OnInit } from '@angular/core';
import { DialogConfig } from '../dialog/dialog-config';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DialogRef } from '../dialog/dialog-ref';
import { DialogService } from '../dialog/dialog.service';
import { MainViewComponent } from '../main-view/main-view.component';
import { Md5 } from 'ts-md5/dist/md5'
import {MatListModule} from '@angular/material/list';
import { FlatsquestionComponent } from '../flatsquestion/flatsquestion.component';

 declare var firebase: any;

@Component({
  selector: 'app-add-member',
  templateUrl: './add-member.component.html',
  styleUrls: ['./add-member.component.css']
})
export class AddMemberComponent implements OnInit {

  flaty:   string;
  members: any[] = [];
  memberHashes: any[] = [];

  alertMessage: String = null;

  constructor(public config: DialogConfig,public dialog: DialogRef, public dialogService: DialogService, public formBuilder: FormBuilder) {
    this.flaty = this.config.data.flaty;
  }

  messageForm: FormGroup;

  async ngOnInit() {
    this.messageForm = this.formBuilder.group({
      email: ['', Validators.required]
    });
    this.memberHashes = await this.getMemberHashes(this.flaty);
    this.members = await this.getMembers(this.memberHashes);
  }

  public finish(){
    this.dialog.close();
    this.dialogService.open(MainViewComponent, this.config);
  }

  public async onSubmit(){
    console.log("addMember to flaty from config");
    let email: string = this.messageForm.get('email').value;
    const md5 = new Md5(); 
    const mailHash = md5.appendStr(email).end();

    let userExists: boolean = await this.checkIfUserExists(mailHash);
    if(userExists){
      //user exits
      let userIsInFlaty: boolean = await this.checkIfInFlaty(mailHash);
      if(!userIsInFlaty){
        this.registerUserInFlaty(mailHash);
        this.ngOnInit();
        this.alertMessage = null;
      } else {
        this.alertMessage = "This user is already in a Flaty"
      }
    } else {
      this.alertMessage = "This user does not exist"
    }
    this.messageForm.reset();
  }

  async leave(){
    this.config.data.flaty = null;

    //set flaty in user entry to null
    firebase.database().ref("/users/").child(this.config.data.userMailHash).update({flaty: null});

    let temp = [];
    let counter = 0;  //used to reset indices
    for(let i=0; i<this.memberHashes.length; i++){
      if(this.memberHashes[i] !== this.config.data.userMailHash && this.memberHashes[i] !== undefined){
        temp[counter++] = this.memberHashes[i];
      }
    }

    this.memberHashes = temp;
    // //remove the user from the flaty
    firebase.database().ref("/flaties/" + this.flaty).update({members: this.memberHashes});

    console.log("leave")
    this.dialog.close();
    this.dialogService.open(FlatsquestionComponent, this.config);

  }

  /**
   * Add user to a flaty.
   * @param mailHash Hash of the mailaddress of the user, that should be added
   */
  async registerUserInFlaty(mailHash: any){
    const flaty = this.config.data.flaty;

    try{
      let members: any = await this.getMemberHashes(flaty);
      members.push(mailHash);

      //add new user in flaty and flaty to user
      firebase.database().ref("/users").child(mailHash).update({flaty: flaty});
      firebase.database().ref("/flaties").child(flaty).update({members: members});
      console.log("registered new user")
    } catch(error) {
      console.log(error)
      this.alertMessage = "Could not add user to Flaty"
    }
  }

  /**
   * Check whether a user exists by its mailHash.
   * @param mailHash of the emailadress of the user.
   */
  async checkIfUserExists(mailHash: any): Promise<boolean> {
    return new Promise(function(resolve, reject) {
      setTimeout(function() {
        firebase.database().ref("/users").on("value", function(snapshot) {
          const userExists = snapshot.child(mailHash).val() !== null;
          resolve(userExists);
        });
      }, 50);
    }); 
  }

  /**
   * Check whether a user is already in an existing flaty.
   * @param userMailHash 
   */
  async checkIfInFlaty(userMailHash: any): Promise<boolean> {
    return new Promise(function(resolve, reject){
      setTimeout(function() {
        firebase.database().ref("/users").on("value", function(snapshot){
          const inFlaty = snapshot.child(userMailHash).val().flaty !== undefined;
          resolve(inFlaty);
        });
      }, 50);
    });    
  }

  /**
   * Get userHashes by a flatyName
   * @param flatyName 
   */
  async getMemberHashes(flatyName: string): Promise<any> {
    return new Promise(function(resolve, reject) {
      setTimeout(function() {
        firebase.database().ref("/flaties").on("value", function(snapshot){
          const flaty = snapshot.child(flatyName).val();
          resolve(flaty.members);
        });
      }, 50);
    });
  }

  /**
   * Get members emails by their mailHahses
   * @param hashes of the users
   */
  async getMembers(hashes: any[]): Promise<any> {
    let array = [];
    return new Promise(function(resolve, reject) {
      setTimeout(function() {
        firebase.database().ref("/users").on("value", function(snapshot){
          for(let i = 0; i<hashes.length; i++){
            let u = snapshot.child(hashes[i]).val();
            if(u !== null){
              array.push(u);
            }
          }
          resolve(array);
        });
      }, 50);
    });
  }
}
